import React from "react"
import '../App.css';
import { Link } from "react-router-dom";
import { blue } from "@material-ui/core/colors";

const About = () => {
    return (
        <section>
            <div style={{ padding: "10px", border: "1px solid #ccc ", borderRadius: "10px", backgroundColor:"#3F51B5"}}>
                <h1  style={{color:"white"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <ol>
                    <li style={{color:"white"}}><strong style={{ width: "100px"}}>Nama:</strong> Nur Qodir Wajid Muharom</li>
                    <li style={{color:"white"}}><strong style={{ width: "100px"}}>Email:</strong> odingajah@gmail.com</li>
                    <li style={{color:"white"}}><strong style={{ width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                    <li style={{color:"white"}}><strong style={{ width: "100px"}}>Akun Gitlab:</strong> @odingajah</li>
                    <li style={{color:"white"}}><strong style={{ width: "100px"}}>Akun Telegram:</strong> @Gogogeq</li>
                </ol>
            </div>
            <br />
            <br />
            <button style={{marginLeft: "1100px", backgroundColor:"#3F51B5", borderRadius:"10px"}}>
                <Link to="/"><a style={{color:"white"}}>Kembali ke Home</a></Link>
            </button>
        </section>
    );
};

export default About;
